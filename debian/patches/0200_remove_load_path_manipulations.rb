Description: do not override $LOAD_PATH
 This ensures that the lib we require is the one installed in debian/ruby-mkrf
Author: Cédric Boutillier <cedric.boutillier@gmail.com>
Last-Update: 2011-12-15

--- a/test/abstract_unit.rb
+++ b/test/abstract_unit.rb
@@ -1,7 +1,8 @@
-$:.unshift(File.dirname(__FILE__) + '/../lib')
+#$:.unshift(File.dirname(__FILE__) + '/../lib')
 
 require 'test/unit'
-require File.dirname(__FILE__) + '/../lib/mkrf'
+#require File.dirname(__FILE__) + '/../lib/mkrf'
+require 'mkrf'
 
 $debug = false
 
@@ -41,4 +42,4 @@
     assert File.exist?(file), "#{file} wasn't created!"
   end
   
-end
\ No newline at end of file
+end
--- a/Rakefile
+++ b/Rakefile
@@ -6,7 +6,7 @@
 #require 'rubygems'
 
 
-$:.unshift(File.dirname(__FILE__) + "/lib")
+#$:.unshift(File.dirname(__FILE__) + "/lib")
 require 'mkrf'
 
 PKG_NAME      = 'mkrf'
